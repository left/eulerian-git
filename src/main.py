# -*- coding: utf-8 -*-
#!/usr/bin/env python2.7

import random
from collections import defaultdict


class Graph(object):
    """Graf skierowany.

    Posiada takie operacje jak dodawani i usuwanie krawędzi,
    sprawdzanie spójności lub test na cykl/drogę Eulera.
    """

    def __init__(self, graph=None):
        self.adjacency_list = {}
        self.in_degree = defaultdict(int)
        self.out_degree = defaultdict(int)
        if graph:
            for v1 in graph:
                if graph[v1]:
                    for v2 in graph[v1]:
                        self.add_edge(v1, v2)
                else:
                    self.add_vertex(v1)

    def copy(self):
        return Graph(self.adjacency_list)

    def get_vertices(self):
        return self.adjacency_list.keys()

    def add_edge(self, v1, v2):
        try:
            if v2 in self.adjacency_list[v1]:
                return
            self.adjacency_list[v1].add(v2)
        except KeyError:
            self.adjacency_list[v1] = set([v2])
        self.add_vertex(v2)
        self.out_degree[v1] += 1
        self.in_degree[v2] += 1

    def add_vertex(self, v):
        if v in self.adjacency_list:
            return
        self.adjacency_list[v] = set()

    def remove_edge(self, v1, v2):
        self.adjacency_list[v1].remove(v2)
        self.out_degree[v1] -= 1
        self.in_degree[v2] -= 1

    def is_eulerian(self):
        in_gt_out = False
        out_gt_in = False
        for v in self.get_vertices():
            if self.in_degree[v] != self.out_degree[v]:
                if abs(self.in_degree[v] - self.out_degree[v]) != 1:
                    return False
                if self.in_degree[v] > self.out_degree[v]:
                    if in_gt_out:
                        return False
                    in_gt_out = True
                else:
                    if out_gt_in:
                        return False
                    out_gt_in = True
            elif self.in_degree[v] == self.out_degree[v] == 0:
                return False
        return in_gt_out == out_gt_in and self.is_connected()

    def has_edge(self, v1, v2):
        try:
            return v2 in self.adjacency_list[v1]
        except KeyError:
            return False

    def has_vertex(self, v):
        return v in self.adjacency_list

    def is_connected(self):
        return ConnectedGraphChecker(self).is_connected()

    def get_edges_count(self):
        return sum(len(edges) for edges in self.adjacency_list.values())

    def is_separated_vertex(self, v):
        return self.in_degree[v] == self.out_degree[v] == 0

    def is_bad_eulerian_vertex(self , v):
        return self.in_degree[v] != self.out_degree[v] or self.is_separated_vertex(v)

    def is_reverse_degree_diff(self, v1, v2):
        return self.out_degree[v1] > self.in_degree[v1] and self.out_degree[v2] < self.in_degree[v2]

    def is_bridge(self, v1, v2):
        if not self.has_edge(v1, v2):
            return False
        return (v1, v2) in Trajan(self).solve()


class RandomEulerianGraphGenerator(object):
    """Generator spójnych grafów skierowanych o dużym prawdopodobieństwie
    posiadania cyklu/drogi Eulera.
    """

    def __init__(self, n, m=0, randomizer=lambda: random.random() > 0.5):
        self.n = n
        self.m = m
        self.randomizer = randomizer

    def generate(self):
        g = self.random_graph()
        g.random = g.copy()
        g = self.repair_to_eulerian(g)
        return g

    def random_graph(self):
        g = Graph()
        groups = []
        vertex_to_group = {}

        for i in xrange(self.n):
            g.add_vertex(i)
            group = set([i])
            groups.append(group)
            vertex_to_group[i] = group

        while len(groups) != 1:
            vertices = g.get_vertices()
            v1 = random.choice(vertices)
            v1_group = vertex_to_group[v1]
            other_vertices = set(vertices)
            other_vertices.difference_update(v1_group)
            v2 = random.choice(list(other_vertices))
            v2_group = vertex_to_group[v2]
            if self.randomizer():
                g.add_edge(v1, v2)
            else:
                g.add_edge(v2, v1)
            v1_group.update(v2_group)
            groups.remove(v2_group)
            for v in v1_group:
                vertex_to_group[v] = v1_group

        vertices = set(g.get_vertices())
        while g.get_edges_count() < self.m and vertices:
            v1 = random.choice(list(vertices))
            other_vertices = vertices - g.adjacency_list[v1] - set([v1])
            if not other_vertices:
                vertices.remove(v1)
                continue
            v2 = random.choice(list(other_vertices))
            g.add_edge(v1, v2)

        return g

    def repair_to_eulerian(self, g):
        direction = False
        for i in xrange(self.n):
            if g.is_bad_eulerian_vertex(i):
                vertices_to_repair = xrange(i + 1, self.n) if direction else xrange(self.n - 1, i, -1)
                direction = not direction
                for j in vertices_to_repair:
                    if g.is_bad_eulerian_vertex(j):
                        self.repair_vertices_pair_to_eulerian(g, i, j)
                        if g.out_degree[i] == g.in_degree[i] and not g.is_separated_vertex(i):
                            break
        return g


    def repair_vertices_pair_to_eulerian(self, g, i, j):
        if g.is_reverse_degree_diff(j, i):
            if g.has_edge(j, i) and not g.is_bridge(j, i):
                g.remove_edge(j, i)
            if g.is_reverse_degree_diff(j, i):
                g.add_edge(i, j)
        elif g.is_reverse_degree_diff(i, j):
            if g.has_edge(i, j) and not g.is_bridge(i, j):
                g.remove_edge(i, j)
            if g.is_reverse_degree_diff(i, j):
                g.add_edge(j, i)
        elif g.is_separated_vertex(i):
            if g.out_degree[j] < g.in_degree[j]:
                g.add_edge(j, i)
            else:
                g.add_edge(i, j)


class Fleury(object):
    """Algorytm Fleurego.

    Znajduje w grafie cykl/drogę Eulera. Korzysta z algorytmu Trajana.
    """

    def __init__(self, graph):
        self.graph = graph

    def solve(self):
        path = []
        if not self.graph.is_eulerian():
            return path
        graph = self.graph.copy()
        for v in graph.get_vertices():
            if graph.in_degree[v] < graph.out_degree[v]:
                break
        path.append(v)
        while len(graph.adjacency_list[v]) > 0:
            w = self._pop_next_vertex(graph, v)
            path.append(w)
            v = w
        # assert set(path) == set(graph.get_vertices())
        return path

    def _pop_next_vertex(self, graph, v):
        for w in graph.adjacency_list[v]:
            if not self._is_bridge(graph, v, w):
                break
        else:
            w = list(graph.adjacency_list[v]).pop()
        graph.remove_edge(v, w)
        return w

    def _is_bridge(self, graph, v, w):
        bridges = Trajan(graph).solve()
        return (v, w) in bridges


class Trajan(object):
    """Algorytm Trajana."""

    class Color:
        WHITE = 1
        GRAY = 2
        BLACK = 3

    def __init__(self, graph):
        self.graph = graph
        self.color = {}
        self.depth = {}
        self.min_depth = {}
        for v in self.graph.get_vertices():
            self.color[v] = Trajan.Color.WHITE
            self.depth[v] = self.min_depth[v] = None

    def solve(self):
        self.time = 0
        self.bridges = set()
        for u in self.graph.get_vertices():
            if self.color[u] == Trajan.Color.WHITE:
                self.component = set()
                self.visit(u)
        return self.bridges

    def visit(self, u):
        self.depth[u] = self.min_depth[u] = self.time
        self.time += 1
        self.component.add(u)
        self.color[u] = Trajan.Color.GRAY
        for v in self.graph.adjacency_list[u]:
            if self.color[v] == Trajan.Color.WHITE:
                self.visit(v)
                self.min_depth[u] = min(self.min_depth[u], self.min_depth[v])
                if self.min_depth[v] > self.depth[u]:
                    self.bridges.add((u, v))
                    self.component.remove(v)
            elif self.color[v] == Trajan.Color.GRAY:
                self.min_depth[u] = min(self.min_depth[u], self.depth[v])
            elif self.color[v] == Trajan.Color.BLACK:
                if v not in self.component:
                    self.bridges.add((u, v))
                else:
                    self.min_depth[u] = min(self.min_depth[u], self.min_depth[v])
        self.color[u] = Trajan.Color.BLACK


class ConnectedGraphChecker(object):
    """Sprawdzanie spójności grafu skierowanego."""

    def __init__(self, graph):
        self.graph = graph
        self.visited_color = {}
        for v in self.graph.get_vertices():
            self.visited_color[v] = None

    def is_connected(self):
        self.components = 0
        self.init_color = 0
        for u in self.graph.get_vertices():
            if self.visited_color[u] is None:
                self.components += 1
                self.init_color += 1
                self.visit(u)
        return self.components == 1

    def visit(self, u):
        self.visited_color[u] = self.init_color
        for v in self.graph.adjacency_list[u]:
            if self.visited_color[v] is not None:
                if self.visited_color[u] != self.visited_color[v]:
                    self.components -= 1
            else:
                self.visit(v)


def read_graph(file):  # @ReservedAssignment
    """Wczytywanie grafu ze standardowego wyjścia."""
    n, m = file.readline().split()
    n, m = int(n), int(m)
    graph = Graph()
    for _ in xrange(m):
        x, y = file.readline().split()
        x, y = int(x), int(y)
        graph.add_edge(x, y)
    return graph


if __name__ == '__main__':
    from sys import stdin
    g = read_graph(stdin)
    print(Fleury(g).solve())
