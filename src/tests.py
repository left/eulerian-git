from unittest import TestCase, main
from StringIO import StringIO
from main import read_graph, Graph, Fleury, Trajan, RandomEulerianGraphGenerator


class GraphTest(TestCase):

    def test_add_edge(self):
        g = Graph()

        g.add_edge(0, 1)

        self.assertEqual(len(g.get_vertices()), 2)
        self.assertEqual(g.adjacency_list, {0: set([1]), 1: set()})
        self.assertEqual(g.in_degree[0], 0)
        self.assertEqual(g.out_degree[0], 1)

    def test_add_edge_twice(self):
        g = Graph()

        g.add_edge(0, 1)
        g.add_edge(0, 1)

        self.assertEqual(len(g.get_vertices()), 2)
        self.assertEqual(g.adjacency_list, {0: set([1]), 1: set()})
        self.assertEqual(g.in_degree[0], 0)
        self.assertEqual(g.out_degree[0], 1)

    def test_remove_edge(self):
        g = Graph({0: [1]})

        g.remove_edge(0, 1)

        self.assertEqual(len(g.get_vertices()), 2)
        self.assertEqual(g.adjacency_list, {0: set(), 1: set()})
        self.assertEqual(g.in_degree[0], 0)
        self.assertEqual(g.out_degree[0], 0)

    def test_is_eulerian_complete_3(self):
        graph = Graph({0: [1, 2], 1: [0, 2], 2: [0, 1]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_two_not_even_vertices(self):
        graph = Graph({0: [1], 1: [0, 2], 2: [1, 3], 3: [2]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_line(self):
        graph = Graph({0: [1], 1: [0, 2], 2: [1]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_star(self):
        graph = Graph({0: [1, 2, 3], 1: [0], 2: [0], 3: [0]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_2_vertices_1_edge(self):
        graph = Graph({0: [1]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_2_vertices_2_edges(self):
        graph = Graph({0: [1], 1: [0]})
        self.assertTrue(graph.is_eulerian())

    def test_is_eulerian_2_vertices_0_edges(self):
        graph = Graph({0: [], 1: []})
        self.assertFalse(graph.is_eulerian())

    def test_is_eulerian_7_vertices_1_separated(self):
        graph = Graph({0: [1, 3], 1: [4, 5], 2: [1], 3: [2], 4: [0], 5: [0], 6: []})
        self.assertFalse(graph.is_eulerian())

    def test_is_connected_complete_3(self):
        graph = Graph({0: [1, 2], 1: [0, 2], 2: [0, 1]})
        self.assertTrue(graph.is_connected())

    def test_is_connected_star(self):
        graph = Graph({0: [1, 2, 3], 1: [0], 2: [0], 3: [0]})
        self.assertTrue(graph.is_connected())

    def test_is_connected_2_vertices_0_edges(self):
        graph = Graph({0: [], 1: []})
        self.assertFalse(graph.is_connected())

    def test_is_connected_double_complete_3_with_bridge(self):
        graph = Graph({0: [1, 2], 1: [0, 2], 2: [0, 1], 3: [4, 5], 4: [3, 5], 5: [3, 4, 0]})
        self.assertTrue(graph.is_connected())

    def test_is_connected_double_2_complete_disconnected(self):
        graph = Graph({0: [1], 1: [0], 2: [3], 3: [2]})
        self.assertFalse(graph.is_connected())

    def test_get_edges_count(self):
        graph = Graph({0: [1], 1: [0]})
        self.assertEqual(graph.get_edges_count(), 2)

    def test_is_separated_vertex(self):
        graph = Graph({0: []})

        self.assertTrue(graph.is_separated_vertex(0))

    def test_is_not_separated_vertex(self):
        graph = Graph({0: [1]})

        self.assertFalse(graph.is_separated_vertex(0))

    def test_is_bad_eulerian_vertex(self):
        graph = Graph({0: [1]})

        self.assertTrue(graph.is_bad_eulerian_vertex(0))

    def test_is_not_bad_eulerian_vertex(self):
        graph = Graph({0: [1], 1: [0]})

        self.assertFalse(graph.is_bad_eulerian_vertex(0))

    def test_is_reverse_degree_diff(self):
        graph = Graph({0: [1]})

        self.assertTrue(graph.is_reverse_degree_diff(0, 1))

    def test_is_bridge(self):
        graph = Graph({0: [1]})

        self.assertTrue(graph.is_bridge(0, 1))


class ReadGraphTest(TestCase):

    def test_read_graph(self):
        stdin = '''3 2
        0 1
        1 2'''
        io = StringIO(stdin)

        g = read_graph(io)

        self.assertEqual(g.adjacency_list, {0: set([1]), 1: set([2]), 2: set()})
        self.assertEqual(len(g.get_vertices()), 3)


class FleuryTest(TestCase):

    def test_solve_2_vertices_1_edge(self):
        graph = Graph({0: [1]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 2)

    def test_solve_2_vertices_2_edges(self):
        graph = Graph({0: [1], 1: [0]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 3)

    def test_solve_3_vertices_3_edges(self):
        graph = Graph({0: [1], 1: [2], 2: [0]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 4)

    def test_solve_3_vertices_2_edges(self):
        graph = Graph({0: [1, 2]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 0)

    def test_solve_complete_3(self):
        graph = Graph({0: [1, 2], 1: [0, 2], 2: [0, 1]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 7)

    def test_solve_4_vertices_4_edges(self):
        graph = Graph({0: [1], 1: [2], 2: [3], 3: [0]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 5)

    def test_solve_3_vertices_5_edges(self):
        graph = Graph({0: [1], 1: [0, 2], 2: [1, 0]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 6)

    def test_solve_double_2_complete_disconnected(self):
        graph = Graph({0: [1], 1: [0], 2: [3], 3: [2]})
        path = Fleury(graph).solve()
        self.assertFalse(path)

    def test_solve_6_verticies_8_edges(self):
        graph = Graph({0: [5], 1: [2, 3], 2: [1, 4], 3: [1], 4: [0], 5: [2]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 9)

    def test_solve_7_vertices_10_edges(self):
        graph = Graph({0: [1, 3], 1: [2, 5], 2: [0], 3: [4, 6], 4: [1], 5: [0], 6: [3]})
        path = Fleury(graph).solve()
        self.assertEqual(len(path), 11)


class TraianTest(TestCase):

    def test_solve_complete_3(self):
        graph = Graph({0: [1, 2], 1: [0, 2], 2: [0, 1]})
        bridges = Trajan(graph).solve()
        self.assertEqual(len(bridges), 0)

    def test_solve_line(self):
        graph = Graph({0: [2], 1: [2], 2: [0, 1]})
        bridges = Trajan(graph).solve()
        self.assertEqual(len(bridges), 0)

    def test_solve_line_with_bridge(self):
        graph = Graph({0: [], 1: [2], 2: [0, 1]})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(2, 0)]))

    def test_solve_star(self):
        graph = Graph({0: [1, 2, 3], 1: [0], 2: [0], 3: [0]})
        bridges = Trajan(graph).solve()
        self.assertEqual(len(bridges), 0)

    def test_solve_double_complete_3(self):
        graph = Graph({0: [1, 2, 5], 1: [0, 2], 2: [0, 1], 3: [4, 5], 4: [3, 5], 5: [0, 3, 4]})
        bridges = Trajan(graph).solve()
        self.assertEqual(len(bridges), 0)

    def test_solve_double_complete_3_with_bridge(self):
        graph = Graph({0: [1, 2, 5], 1: [0, 2], 2: [0, 1], 3: [4, 5], 4: [3, 5], 5: [3, 4]})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(0, 5)]))

    def test_solve_double_2_complete_with_bridge(self):
        graph = Graph({0: [1], 1: [0, 2], 2: [3], 3: [2]})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(1, 2)]))

    def test_solve_5_vertices_5_egdes_3_bridges(self):
        graph = Graph({1: [2, 3], 2: [4], 3: [1], 4: [0]})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(1, 2), (2, 4), (4, 0)]))

    def test_solve_3_vertices_4_edges_2_strong_components(self):
        graph = Graph({1: [2, 3], 2: [1, 3], 3: []})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(2, 3), (1, 3)]))

    def test_solve_7_vertices_0_edges(self):
        graph = Graph({0: [1, 3], 1: [2, 5], 2: [0], 3: [4, 6], 4: [1], 5: [0], 6: []})
        bridges = Trajan(graph).solve()
        self.assertSetEqual(bridges, set([(3, 6)]))


class RandomEulerianGraphGeneratorTest(TestCase):

    def test_generate_eulerian_graph(self):
        graph = RandomEulerianGraphGenerator(n=2).generate()

        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_a(self):
        graph = Graph({0: [1, 3], 1: [0, 3], 2: [0, 3], 3: [2]})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_b(self):
        graph = Graph({0: [1, 2, 3], 1: [2], 2: [], 3: [0]})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_c(self):
        graph = Graph({0: [2], 1: [], 2: [], 3: [0, 2]})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_d(self):
        graph = Graph({0: [1, 2], 1: [2], 2: [], 3: [2]})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_e(self):
        graph = Graph({0: [2], 1: [2], 2: [0], 3: [0, 1, 2]})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_repair_to_eulerian_4_vertices_f(self):
        graph = Graph({0: [], 1: [], 2: [], 3: []})
        RandomEulerianGraphGenerator(n=len(graph.get_vertices())).repair_to_eulerian(graph)
        self.assertTrue(graph.is_eulerian())

    def test_edges_count_with_m_equal_0(self):
        graph = RandomEulerianGraphGenerator(n=10, m=0, randomizer=lambda: True).random_graph()

        self.assertEqual(graph.get_edges_count(), len(graph.get_vertices()) - 1)

    def test_edges_count_with_m_not_equal_0(self):
        graph = RandomEulerianGraphGenerator(n=10, m=20, randomizer=lambda: True).random_graph()

        self.assertEqual(graph.get_edges_count(), 20)


if __name__ == '__main__':
    main()
