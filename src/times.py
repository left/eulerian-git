#! /usr/bin/env python

from main import Fleury, RandomEulerianGraphGenerator
from time import time
from collections import OrderedDict
import sys
import argparse

def count_const(size, edges, duration):
    return edges * (size + edges) / duration

if __name__ == '__main__':
    sys.setrecursionlimit(100000)

    parser = argparse.ArgumentParser(description='Fleury tests.')
    parser.add_argument('--min-n', metavar='N', default=500, type=int, help='min graph size (default: %(default)s)')
    parser.add_argument('--max-n', metavar='N', default=1500, type=int, help='max graph size (default: %(default)s)')
    parser.add_argument('--step', metavar='S', default=100, type=int, help='graph size step (default: %(default)s)')
    parser.add_argument('--factor', metavar='F', default=0, type=float,
                        help='graph sizes to edges factor (default: %(default)s)')
    parser.add_argument('--repetitions', metavar='R', default=10, type=int,
                        help='repetitions for one size (default: %(default)s)')
    args = parser.parse_args()

    stats = OrderedDict()

    print('Run Fleury algorithm, graph size = ({0}, {1}), step = {2}:\n'.format(args.min_n, args.max_n, args.step))

    try:
        for size in range(args.min_n, args.max_n, args.step):
            stats[size] = []
            m = size * (size - 1) * args.factor
            for repetition in range(1, args.repetitions + 1):
                graph = RandomEulerianGraphGenerator(n=size, m=m).generate()
                while not graph.is_eulerian():
                    graph = RandomEulerianGraphGenerator(n=size, m=m).generate()
                start_time = time()
                path = Fleury(graph).solve()
                duration = time() - start_time
                edges = graph.get_edges_count()
                const = count_const(size, edges, duration)
                stats[size].append({
                    'edges': edges,
                    'const': const,
                    'duration': duration
                })
                print(' - size: {0}, repetition: {1}, edges: {2}, duration: {3:.2f}s'.format(size, repetition, edges, duration))
    except KeyboardInterrupt:
        pass
        if not stats[size]:
            del stats[size]

    if stats:
        print('\nFleury algorithm stats:\n')
        avg_const = sum([sum(size_stats['const'] for size_stats in sizes_stats)/ len(sizes_stats) for sizes_stats in stats.values()]) / len(stats)

        for size, sizes_stats in stats.items():
            normC = 1.0 / (sum(size_stats['const'] for size_stats in sizes_stats) / len(sizes_stats) / avg_const)
            avgEdges = sum(size_stats['edges'] for size_stats in sizes_stats) / len(sizes_stats)
            avgDuration = sum(size_stats['duration'] for size_stats in sizes_stats) / len(sizes_stats)
            print(' - size: {0}, avgEdges: {1}, avgDuration: {2:.2f}s, C/avgC: {3:.3f}'.format(size, avgEdges, avgDuration, normC))

